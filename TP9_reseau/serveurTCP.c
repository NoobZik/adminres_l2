/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   serveurTCP.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/29 17:47:48 by NoobZik           #+#    #+#             */
/*   Updated: 2018/03/29 20:09:55 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * Changement des variables effectué
 *
 * 1- short port en uint16_t
 * Raison : un htons recoit en paramètre un uint16_t
 * Les autres instances seront alors casté en uint16_t
 *
 * 2- int ret en ssize_t ret
 * Raison : Read/Write renvoie une valeur de type ssize_t
 * Pour afficher un ssize_t, il faut faire %zu
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define NBECHANGE 3

char * id = 0;
uint16_t port = 0;
int sock = 0; /* socket de communication */
int nb_reponse = 0;

int main(int argc, char ** argv) {
	struct sockaddr_in serveur; /* SAP du serveur */
  ssize_t ret;

	if (argc != 3) {
		fprintf(stderr,"usage: %s id port\n",argv[0]);
		exit(1);
	}
	id = argv[1];
	port = (uint16_t) atoi(argv[2]);
  /**
   * Socket :
   * AF_NET = Adresse IP
   * SOCK_STREAM
   * 0 pour définir automatiquement le protocole
   */
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    /*htonl : pour loes entiers longs ; htons : prend un entier court */
		fprintf(stderr,"%s: socket %s\n", argv[0],strerror(errno));
		exit(1);
	}
  /**
   * Remplissage des champs de serveur qui est de type sockaddr_in.
   */
	serveur.sin_family = AF_INET;
	serveur.sin_port = htons (port);
	serveur.sin_addr.s_addr = htonl (INADDR_ANY);  /*pour le convertir a un entier long */

  /**
   * bind permet d'associer une adresse à la socket
   */
	if (bind(sock, (struct sockaddr *) &serveur,sizeof (serveur)) < 0) {
		fprintf(stderr,"%s: bind %s\n", argv[0],strerror(errno));
		exit(1);
	}

  /**
   * listen() permet de mettre le "sock" en mode passif. Ce socket sera utilisé
   * pour accepter les requetes de connections avec la fonction accept().
   *
   * Le deuxieme paramètre determine le nombre maximale de connection en attente.
   */
	if (listen(sock,5) != 0) {/*une file d'attente */
		fprintf(stderr,"%s: listen %s\n", argv[0],strerror(errno));
		exit(1);
	}
	while (1) {
		struct sockaddr_in client; /* SAP du client */
		socklen_t len = sizeof(client);
		int sock_pipe; /* socket de dialogue */
		int nb_question;

    /**
     * une fontion bloquante : elle va recuperer un client qui est en file
     * d'attente de serveur et elle va le traiter; struct addr : c'est la meme
     * que struct in_addr : elle va servir de la structure de client pour
     * remplir le serveur. Faut toujours faire le caste
     */
    sock_pipe = accept(sock , (struct sockaddr *) &client, &len);
		for (nb_question = 0;nb_question < NBECHANGE; nb_question++) {
			char buf_read[256], buf_write[256];
      /**
       * si il va lire 50 ok il renvoie 50
       */
			ret = read(sock_pipe,buf_read,256);
			if (ret <= 0) {
			printf("%s: read=%zu: %s\n", argv[0], ret, strerror(errno));
			break;
		}
    /**
     * Ici on affiche l'adresse du client, son port et le buffer lu.
     */
		printf("serveur %s recu de (%s,%4d):%s\n",
    id, inet_ntoa(client.sin_addr), htons(client.sin_port),buf_read);

		sprintf(buf_write,"#%2s=%03d#", id,nb_reponse++);
    /**
     * Ecriture du buffer vers le sock pipe avec une limite de 256 octect
     */
		ret = write(sock_pipe,&buf_write,256);
		if (ret <= 0) {
			printf("%s: write=%zu: %s\n", argv[0], ret, strerror(errno));
			break;
		}
		sleep(2);
	}
	close(sock_pipe);
	}
	return 0;
}
