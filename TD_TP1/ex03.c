#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

volatile char theChar = '\0';
volatile char afficher = 0;

void *lire (void *name);
void *affichage (void * name);

void *lire (void *name) {
  (void) name;
  do {
    while (afficher == 1);
    theChar = (char) getchar();
    afficher = 1;
  }
  while (theChar != 'F') ;
  return NULL;
}

void *affichage (void *name) {
  (void) name;
  int cpt = 0;
  do {
    while (afficher == 0) cpt++;
    printf ("cpt = %d, car = %c \n", cpt, theChar) ;
    afficher = 0;
  }
  while (theChar != 'F');
  return NULL ;
}

int main (void) {
  pthread_t filsA, filsB;
  if (pthread_create(&filsA, NULL, affichage, NULL)) {
    perror ("pthre ad_creat e ");
    exit (EXIT_FAILURE);
  }

  if (pthread_create(&filsB, NULL, lire, NULL)) {
    perror ("pthre ad_creat e");
    exit (EXIT_FAILURE);
  }
  if (pthread_join (filsA , NULL))
    perror ("pthread_join");
  if (pthread_join (filsB, NULL))
    perror ("pthread_join");
  printf ( "Fin du pere\n");
  return (EXIT_SUCCESS) ;
}
