/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex04.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 17:40:10 by NoobZik           #+#    #+#             */
/*   Updated: 2018/01/18 18:22:46 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>

int main(int argc, char const *argv[]) {
  if (argc == 1)
    return 1;
  int i = -1;
  int nbmax = atoi(argv[1]);
  char *next = (char *) argv[1];

  pid_t p1;

  while (++i < nbmax) {
    if ((p1 = fork()) == 0) {
      execv("lance", &argv[3]);
      exit(0);
    }
  }
  return 0;
}
