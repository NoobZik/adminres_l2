/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   td2.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/25 17:19:41 by NoobZik           #+#    #+#             */
/*   Updated: 2018/01/25 17:29:31 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _HEADER_TD2_H_
#define _HEADER_TD2_H_

#define N 5

#include "Stuctures/td2.h"

void  generation_aleatoire (int matrice[N][N]);
void  imprime              (int matrice[N][N]);
void *somme                (void *arg);


#endif //_HEADER_TD2_H_
