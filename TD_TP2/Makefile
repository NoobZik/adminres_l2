# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/12/05 10:50:49 by NoobZik           #+#    #+#              #
#    Updated: 2017/12/05 15:58:47 by NoobZik          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#
# CONFIGURATIONS
#

NAME := exerice.exe

OBJDIR := Objects
OUTDIR := Outputs
SRCDIR := Sources
HDRDIR := Headers
RDSDIR := Ressources

#
# DO NOT EDIT FORWARD
#

# Compilation settings

WARNINGS := -Wall -Wextra -pedantic -Wshadow -Wpointer-arith -Wcast-align \
			-Wwrite-strings -Wmissing-prototypes -Wmissing-declarations \
			-Wredundant-decls -Wnested-externs -Winline -Wno-long-long \
			-Wuninitialized -Wconversion -Wstrict-prototypes

CFLAGS ?= -std=gnu99 -g $(WARNINGS) -fpic

# Options

ifeq ($(VERBOSE), 1)
		SILENCER :=
else
		SILENCER := @
endif

ifeq ($(DEBUG_BUILD), 1)
		CFLAGS +=-DDEBUG_BUILD
endif

# Compilation command

all: init $(NAME)

# Automated compilator

# Compilator

SRCF := $(shell find ./$(SRCDIR) -name "*.c" -type f | cut -sd / -f 3- | tr '\n' ' ')
SRCS := $(patsubst %, $(SRCDIR)/%, $(SRCF))
OBJS := $(patsubst %, $(OBJDIR)/%, $(SRCF:c=o))

# Compilation output configurations

CFLAGS += -MMD -MP
DEPS := $(patsubst %, $(OBJDIR)/%, $(SRCF:c=d))

$(NAME): $(OBJS)
	$(SILENCER)mkdir -p $(OUTDIR)
	$(SILENCER)$(CC) $(CFLAGS) -o $(OUTDIR)/$(NAME) $^

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(SILENCER)mkdir -p $(OBJDIR)
	$(SILENCER)$(CC) $(CFLAGS) -c -o $@ $<

# Helpers command

init:
	$(SILENCER)mkdir -p $(OBJDIR)
	$(SILENCER)mkdir -p $(SRCDIR)
	$(SILENCER)mkdir -p $(HDRDIR)
	$(SILENCER)mkdir -p $(RDSDIR)
	$(SILENCER)cd ./Sources && find . -type d -exec mkdir -p ../$(OBJDIR)/{} \;
	$(SILENCER)cd ..

clean:
	clear
	$(SILENCER)find . -name "*.o" -type f -delete
	$(SILENCER)find . -name "*.d" -type f -delete

fclean: clean
	$(SILENCER)$(RM) -rf $(OUTDIR)
	$(SILENCER)$(RM) -rf $(OBJDIR)

re: fclean all

run:
	$(OUTDIR)/$(NAME)

.PHONY: re fclean clean init all

-include $(DEPS)
