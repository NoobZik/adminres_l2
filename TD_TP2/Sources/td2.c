/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   td2.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/25 17:15:40 by NoobZik           #+#    #+#             */
/*   Updated: 2018/01/25 18:48:46 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include "../Headers/td2.h"

/**
 * [generation_aleatoire  description]
 * @param [name] [description]
 * @return [description]
 */
void  generation_aleatoire (int matrice[N][N]) {
  int i;
  int j;
  i = j = -1;

  while (++i < N) {
    while (++j < N) {
      matrice[i][j] = rand() % 100;
    }
    j = -1;
  }
}

/**
 * [imprime               description]
 * @param [name] [description]
 * @return [description]
 */
void  imprime              (int matrice[N][N]) {
  int i;
  int j;
  i = j = -1;

  while (++i < N) {
    printf("[ ");
    while (++j < N) {
      printf("%d ", matrice[i][j]);
    }
    puts("]");
    j = -1;
  }
}

/**
 * [somme                description]
 * @param arg [description]
 * @return [description]
 */
void  *somme               (void *arg) {
  struct s *temp = (struct s*) arg;
  *temp->z = temp->x + temp->y;
  return NULL;
}
