/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/25 17:20:06 by NoobZik           #+#    #+#             */
/*   Updated: 2018/01/25 18:48:46 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../Headers/td2.h"

int main(int argc, char const *argv[]) {
  (void) argc;
  (void) argv;

  srand((unsigned int) time(NULL));

  int matA[N][N];
  int matB[N][N];
  int matC[N][N];

  generation_aleatoire(matA);
  generation_aleatoire(matB);

  imprime(matA);
  puts("");
  imprime(matB);

  int i, j;
  i = j = -1;
  while (++i < N) {
    while (++j < N) {
      struct s *tmp = (struct s*) malloc(sizeof(struct s));
      if (pthread_create(&tid ,NULL, somme, tmp));
      tmp->x = matA[i][j];
      tmp->y = matB[i][j];
      tmp->z = &matC[i][j];
      (void) somme(tmp);
      free(tmp);
    }
    j = -1;
  }

  puts("");
  imprime(matC);
  return 0;
}
