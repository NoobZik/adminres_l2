/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex01.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/15 15:54:25 by NoobZik           #+#    #+#             */
/*   Updated: 2018/03/02 14:55:37 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * Rakib Sheikh : 11502605
 * Hakim MBae : 11507091
 */

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdint.h>
#include <time.h>
#include <assert.h>

/**
 * Structure d'argment pour faire passer le tableau stat et sa taile dans le
 * thread
 */
typedef struct arg_t {
  struct stat *buffer;
  off_t        size;
}              arg_t;

void *largest_size (void *source);
void *recent_change (void *source);


/** GLOBAL VARIABLE **/

struct stat *tabStat;
arg_t       *arg_buffer;

/** Ususal functions **/

/**
 * Retourne l'indice de tableau dont le fichier ayant la plus grosse taille
 * @param source arg_t (tabStat + Size)
 * @return -1 si vide, sinon indice
 */
void *largest_size (void *source) {
  off_t largest;
  off_t *result = 0;
  arg_t *source_casted = (arg_t *) source;
  struct stat *buffering = (struct stat *) source_casted->buffer;
//  void *result_return;
  int i = 0;
  if (!source_casted->size) {
//    puts("Pas de fichier");
    return (void *) -1;
  }
  largest = buffering[i].st_size;
  printf("largest : %ld\n", largest);

//  printf("source_casted->size : %d\n", (int) source_casted->size);
  while (++i < source_casted->size) {
      if(largest >= buffering[i].st_size) {
        continue;
      }
      else {
        largest = buffering[i].st_size;
        *result = i;
      }
  }

  printf("I vaut : %d\n", i);
  printf("Dans largest_size, le resultat avant retour est %ld\n", (intptr_t) result);
  return result;
}


/**
 * Retourne l'indice de tableau dont le fichier ayant la derniere modification
 * @param source arg_t (tabStat + Size)
 * @return -1 si vide, sinon indice
 */
/*
void *recent_change (void *source) {
  assert(source);
  time_t largest;
  int result;
  arg_t *source_casted = (arg_t *) source;
  struct stat *buffering = (struct stat *) source_casted->buffer;
  void *result_return;
  int i = 0;
  if (!source_casted->size) {
    return (void *) -1;
  }
  largest = buffering[i].st_mtime;
  result = 0;
  while (++i < source_casted->size) {
      if(largest > buffering[i].st_mtime) {
        continue;
      }
      else {
        largest = buffering[i].st_mtime;
      }
  }
  result_return = &result;
  return result_return;
}

*/
/**
 * Assuming that argv containt the file list.
 */

/* Short typing struct */
typedef struct stat stat_t;

int main(int argc, char const *argv[]) {
  if (argc < 2) {
    puts("No argments not permitted (need files name), exiting...");
    exit(0);
  }

  int i, j;
  int x;
  pthread_t t1;
//  pthread_t t2;
  i = 0;
  j = 0;

  tabStat = (stat_t *) malloc(sizeof(stat_t) * (long unsigned int)(argc-1));
  arg_buffer = (arg_t *) malloc(sizeof(arg_t));

  assert(argv[1]);

  while ((++i < argc-1) && j++) {
    assert(&tabStat[j] && argv[i]);
    if(stat(argv[i], &tabStat[j]) == -1) {
      i++;
    }
  }
  assert(&tabStat[i]);
  puts("Success");
  assert(arg_buffer);

  arg_buffer->buffer = tabStat;
  arg_buffer->size = argc-1;
  assert(arg_buffer->buffer);
  assert(arg_buffer->size);
  puts("Success");

  pthread_create(&t1, NULL, largest_size, arg_buffer);
//  pthread_create(&t2, NULL, recent_change, arg_buffer);

  if (pthread_join(t1, (void **) &x) != -1) {
    printf("L'indice du fichier le plus gros est %ld\n", (intptr_t) x);
  }

/*  if (pthread_join(t2, &x) != -1) {
    printf("L'indice du fichier dernierement modifier est %ld\n", (intptr_t) x);
  }
*/

  /* Discard volatile fix*/
  free((void *)tabStat);
  free(arg_buffer);
  return 0;
}
