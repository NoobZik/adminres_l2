/* serveur_UDP.c (serveur UDP) */
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>


 char* id = 0;
 short port = 0;
 int sock = 0; /* socket de communication */
 int nb_reponse = 0;

 int main(int argc, char** argv) {
     int ret;
     struct sockaddr_in serveur; /* SAP du serveur */

     if (argc!=3) {
         fprintf(stderr,"usage: %s id port\n", argv[0]);
         exit(1);
         }
     id = argv[1];
     port = atoi(argv[2]);
     if ((sock = socket(...............)) == -1) {
          fprintf(stderr,"%s: socket %s\n", argv[0],strerror(errno));
          exit(1);
          }
     serveur.sin_family = ...............;
     serveur.sin_port = ...............;
     serveur.sin_addr.s_addr = ...............);
     if (bind(...............) < 0) {
         fprintf(stderr,"%s: bind %s\n", argv[0],strerror(errno));
         exit(1);
         }

     while (1) {
        struct sockaddr_in client; /* SAP du client */
        int client_len = sizeof(client);
        char buf_read[1<<8], buf_write[1<<8];

        ret = recvfrom(...............);

        if (ret <= 0) {
            printf("%s: recvfrom=%d: %s\n", argv[0],ret,strerror(errno));
            continue;
            }

         printf("serveur %s recu le msg %s de %s:%d\n", id,buf_read, ...............);
         sprintf(buf_write, "serveur#%2s reponse%03d#", id,nb_reponse++);
         ret = sendto(...............);
         if (ret <= 0) {
             printf("%s: sendto=%d: %s\n", argv[0],ret,strerror(errno));
             continue;
             }
         sleep(2);
         }
     return 0;
     }
